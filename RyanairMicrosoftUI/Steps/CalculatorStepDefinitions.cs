﻿using TechTalk.SpecFlow;
using RyanairMicrosoftUI.Helpers;
using NUnit.Framework;

namespace RyanairMicrosoftUI.Steps
{
    [Binding]
    public sealed class CalculatorStepDefinitions
    {

        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;


        [BeforeScenario]
        public void openCalculator()
        {
            ReusableCommands.startCalc();
        }
        [AfterScenario]
        public void closeCalculator()
        {
            ReusableCommands.closeCalc();
        }
        public CalculatorStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

       
        [Given(@"Open calculator and navigate to (.*) mode")]
        public void GivenOpenCalculatorAndNavigateToProgrammerMode(string mode)
        {
            ReusableCommands.SetCalculatorMode(mode);
        }

        [Given(@"Enter the number is (.*)")]
        public void GivenEnterTheNumberIs(string number)
        {
            ReusableCommands.PressOperator("clear");
            ReusableCommands.ClickOnNumber(number.ToString());
        }

        [When(@"Press the (.*) button")]
        public void WhenPressTheEqualsButton(string opr)
        {
            ReusableCommands.PressOperator(opr);
        }
        [When(@"Selected first day of month from Date from Calender")]
        public void WhenSelectedFirstDayOfMonthFromDateFromCalender()
        {
            ReusableCommands.ClickByAccessibilityId("DateDiff_FromDate");
            ReusableCommands.SelectDate("1");
        }

        [When(@"Selected eighth day of month from Date from Calender")]
        public void WhenSelectedEighthDayOfMonthFromDateFromCalender()
        {
            ReusableCommands.waitForSecond(3);
            ReusableCommands.ClickByAccessibilityId("DateDiff_ToDate");
            ReusableCommands.SelectDate("8");

        }
        [When(@"Enter the number is (.*)")]
        public void WhenEnterTheNumber(int number)
        {
            ReusableCommands.ClickOnNumber(number.ToString());
        }

        [Then(@"HEX, DEC, OCT, and BIN values should be generated and the result should be  Hex = (.*) , DEC=(.*) , OCT =(.*) , BIN =(.*)")]
        public void ThenHEXDECOCTAndBINValuesShouldBeGeneratedAndTheResultShouldBeHexADECOCTBIN(string Hex, string DEC, string OCT, string BIN)
        {
            Assert.AreEqual("HexaDecimal " + Hex, ReusableCommands.GetTextByAccessibilityId("hexButton"));
            Assert.AreEqual("Decimal " + DEC, ReusableCommands.GetTextByAccessibilityId("decimalButton"));
            Assert.AreEqual("Octal " + OCT, ReusableCommands.GetTextByAccessibilityId("octolButton"));
            Assert.AreEqual("Binary " + BIN, ReusableCommands.GetTextByAccessibilityId("binaryButton"));
        }

        [Then(@"Square root of given number (.*) should be (.*)")]
        public void ThenSquareRootOfGivenNumberShouldBe(int num1, int num2)
        {
            string sqrt = ReusableCommands.GetTextByAccessibilityId("CalculatorResults");
            Assert.AreEqual("Display is " + num2, sqrt);
        }
        [Then(@"Differnce between date should be (.*) days and (.*) week")]
        public void ThenDiffernceBetweenDateShouldBeDaysAndWeek(int num1, int num2)
        {
            string diffWeek = "Difference "+ num2 + " week";
            string diffDays = num1 + " days";
            //Assert.AreEqual(diffWeek, ReusableCommands.GetTextByAccessibilityId("DateDiffAllUnitsResultLabel"));
            Assert.AreEqual(diffDays, ReusableCommands.GetTextForDays());
        }
        [Then(@"The Result should be (.*)")]
        public void ThenAdditionOfTwoNumberShouldBe(int number)
        {
            Assert.AreEqual("Display is " + number, ReusableCommands.GetTextByAccessibilityId("CalculatorResults"));
        }
    }
}
