﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;

namespace RyanairMicrosoftUI.Helpers
{
    public class ReusableCommands
    {
        public static Process CalculatorProcess;
        public static AutomationElement calculatorAutomationElement;
        public static void startCalc()
        {
            try
            {
                CalculatorProcess = Process.Start("Calc.exe");
              
                int ct = 0;
                do
                {
                    calculatorAutomationElement = AutomationElement.RootElement.FindFirst
                (TreeScope.Children, new PropertyCondition(AutomationElement.NameProperty,
                "Calculator"));

                    ++ct;
                    Thread.Sleep(100);
                } while (calculatorAutomationElement == null && ct < 50);

               
                if (calculatorAutomationElement == null)
                {
                    throw new InvalidOperationException("Calculator must be running");
                }
                MaximizeWindow();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occurred while opening calculator. Error is : " + ex.Message);
            }
        }
        public static void MaximizeWindow()
        {
            if(calculatorAutomationElement.FindFirst
        (TreeScope.Descendants, new PropertyCondition
        (AutomationElement.NameProperty, "Restore Calculator")) == null)
            {
                ClickByName("Maximize Calculator");
            }
        }

        public static InvokePattern GetInvokePattern(AutomationElement element)
        {
            return element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
        }
        public static AutomationElement GetFunctionButton(string functionName)
        {
            AutomationElement functionButton = calculatorAutomationElement.FindFirst
            (TreeScope.Descendants, new PropertyCondition
            (AutomationElement.NameProperty, functionName));

            if (functionButton == null)
            {
                throw new InvalidOperationException("No function button found with name: " +
                functionName);
            }

            return functionButton;
        }
       
        public static void ClickByAccessibilityId(string element)
        {
            GetInvokePattern(calculatorAutomationElement.FindFirst
           (TreeScope.Descendants, new PropertyCondition
           (AutomationElement.AutomationIdProperty, element))).Invoke();
        }
        public static void ClickByName(string element)
        {

            GetInvokePattern(calculatorAutomationElement.FindFirst
        (TreeScope.Descendants, new PropertyCondition
        (AutomationElement.NameProperty, element))).Invoke();
            waitForSecond(2);
        }
        public static void ClickByClassName(string element)
        {

            GetInvokePattern(calculatorAutomationElement.FindFirst
        (TreeScope.Descendants, new PropertyCondition
        (AutomationElement.ClassNameProperty, element))).Invoke();
            waitForSecond(2);
        }
        public static string GetTextByAccessibilityId(string element)
        {

            var result = calculatorAutomationElement.FindFirst
                (TreeScope.Descendants, new PropertyCondition(AutomationElement.AutomationIdProperty,
                element)).GetCurrentPropertyValue(AutomationElement.NameProperty);
            return result.ToString().Trim();

        }
        public static void ClickOnNumber(string number)
        {
            char[] arr = number.ToCharArray();
            foreach (char num in arr)
            {
                ClickByAccessibilityId("num" + num + "Button");

            }

        }
        public static void SelectDate(string startDate)
        {
          var  elements = calculatorAutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition
        (AutomationElement.ClassNameProperty, "CalendarViewDayItem")
        );
           foreach(AutomationElement el in elements)
            {
                if(el.GetCurrentPropertyValue(AutomationElement.NameProperty).ToString() == startDate)
                {
                    waitForSecond(5);
                    SelectionItemPattern select = (SelectionItemPattern)el.GetCurrentPattern(SelectionItemPattern.Pattern);
                    select.Select();
                    waitForSecond(5);
                    break;
                 
                }
            }
        }

       
  

        public static void InvokeFunction(string functionName)
        {
            GetInvokePattern(GetFunctionButton(functionName)).Invoke();
        }

        public static void PressOperator(string opr)
        {
            if (opr == "equals")
            {
                ClickByAccessibilityId("equalButton");
            }
            else if (opr == "add")
            {
                ClickByAccessibilityId("plusButton");
            }
            else if (opr == "minus")
            {
                ClickByAccessibilityId("minusButton");
            }
            else if (opr == "multiply")
            {
                ClickByAccessibilityId("multiplyButton");
            }
            else if (opr == "divide")
            {
                ClickByAccessibilityId("divideButton");
            }

            else if (opr == "clear")
            {
                ClickByAccessibilityId("clearButton");
            }
            else if (opr == "memoryplus")
            {
                ClickByAccessibilityId("MemPlus");
            }
            else if (opr == "memoryrecall")
            {
                ClickByAccessibilityId("MemRecall");
            }
            else if (opr == "square root")
            {
                ClickByAccessibilityId("squareRootButton");
            }
            else if (opr == "history")
            {
               // ClickByAccessibilityId("HistoryLabel");
                waitForSecond(2);
                ClickByClassName("ListViewItem");
            }
        }

        public static string GetTextForDays()
        {
            string dayDifference = "";
            var days = calculatorAutomationElement.FindAll
                (TreeScope.Descendants, new PropertyCondition(AutomationElement.ClassNameProperty,
                "TextBlock"));
             
            foreach (AutomationElement el in days)
            {
                if (el.GetCurrentPropertyValue(AutomationElement.NameProperty).ToString().Contains(" days"))
                {

                    dayDifference = el.GetCurrentPropertyValue(AutomationElement.NameProperty).ToString();
                }
            }

            return dayDifference; //days[4].Text;


        }
        public static void SetCalculatorMode(string mode)
        {

            ClickByAccessibilityId("TogglePaneButton");
            waitForSecond(2);
            if (mode == "programmer")
                ClickByAccessibilityId("Programmer");
            else if (mode == "scientific")
                ClickByAccessibilityId("Scientific");
            else if (mode == "date calculation")
            {
                ClickByAccessibilityId("Date");
                waitForSecond(2);
                
            }
            else
                ClickByAccessibilityId("Standard");
        }
        
        public static void waitForSecond(int timeInSecond)
        {
            timeInSecond = timeInSecond * 1000;
            Thread.Sleep(timeInSecond);

        }

        public static void closeCalc()
        {
            try
            {
                if (CalculatorProcess != null)
                {
                    Process[] _proceses = null;
                    _proceses = Process.GetProcessesByName("Calculator");
                    foreach (Process proces in _proceses)
                    {
                        proces.Kill();
                    }
                    CalculatorProcess.Dispose();
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error in closing calculator. Error is :" + ex.Message);
            }
        }
    }
}
