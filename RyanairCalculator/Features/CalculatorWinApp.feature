﻿Feature: CalculatorWinApp



Scenario: Verify HEX, DEC, OCT, and BIN values are correctly populating in Programmer mode.
	Given Open calculator and navigate to programmer mode
	And Enter the number is 10
	When Press the equals button
	Then HEX, DEC, OCT, and BIN values should be generated and the result should be  Hex = A , DEC=10 , OCT =1 2 , BIN =1 0 1 0

Scenario: Verify calculator operation in Scientific mode.
	Given Open calculator and navigate to scientific mode
	And Enter the number is 4
	When Press the square root button
	Then Square root of given number 4 should be 2

Scenario: Verify calculator operation in date calculation mode.
	Given Open calculator and navigate to date calculation mode
	When Selected first day of month from Date from Calender
	And  Selected eighth day of month from Date from Calender
	Then Differnce between date should be 7 days and 1 week

Scenario: Verify calculator operation in Standard mode.
	Given Open calculator and navigate to standard mode
	And Enter the number is 4
	When  Press the add button
	And  Enter the number is 5
	And Press the equals button
	Then The Result should be 9
	When Press the clear button
	And Press the history button
	Then The Result should be 9
	When Press the equals button
	And  Press the memoryplus button
	And Press the clear button
	And Enter the number is 4
	And  Press the divide button
	And  Enter the number is 2
	And Press the equals button
	And Press the memoryplus button
	And Press the memoryrecall button
	Then The Result should be 11











