﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Support.UI;
using System;

namespace RyanairCalculator.Helpers
{
    public class ReusableCommands
    {
        private static WindowsDriver<WindowsElement> driver;
        public static void startCalc()
        {
            try
            {
                AppiumOptions options = new AppiumOptions();
                options.AddAdditionalCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
                options.AddAdditionalCapability("deviceName", "WindowsPC");
                driver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), options);
                waitForSecond(3);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error occurred while opening calculator. Error is : "+ex.Message);
            }
        }
        public static void ClickOnNumber(string number)
        {
            char[] arr = number.ToCharArray();
            foreach (char num in arr)
            {
                ClickByAccessibilityId("num" + num + "Button");
             
            }
        }
        public static void ClickByAccessibilityId(string element)
        {
           
            driver.FindElementByAccessibilityId(element).Click();
            waitForSecond(2);
        }
        public static void ClickByName(string element)
        {
           
            driver.FindElementByName(element).Click();
            waitForSecond(2);
        }
        public static void ClickByClassName(string element)
        {

            driver.FindElementByClassName(element).Click();
            waitForSecond(2);
        }
        public static string GetTextByAccessibilityId(string element)
        {
            return driver.FindElementByAccessibilityId(element).Text.Trim();

            
        }
        public static string GetTextForDays()
        {
            string dayDifference = "";
            var days =driver.FindElementsByClassName("TextBlock");
            foreach(var el in days)
            {
                if(el.Text.Contains(" days"))
                {

                    dayDifference = el.Text;
                }
            }

            return dayDifference; //days[4].Text;


        }
        public static void SetCalculatorMode(string mode)
        {
            waitForSecond(2);
            ClickByAccessibilityId("TogglePaneButton");
            waitForSecond(2);
            if (mode == "programmer")
                ClickByAccessibilityId("Programmer");
            else if (mode == "scientific")
                ClickByAccessibilityId("Scientific");
            else if (mode == "date calculation")
            {
                ClickByAccessibilityId("Date");
                ClickByAccessibilityId("TogglePaneButton");
                waitForSecond(2);
                ClickByAccessibilityId("Date");
            }
            else
                ClickByAccessibilityId("Standard");
        }
        public static void PressOperator(string opr)
        {
            if(opr == "equals")
            {
                ClickByAccessibilityId("equalButton");
            }
            else if(opr == "add")
            {
                ClickByAccessibilityId("plusButton");
            }
            else if (opr == "minus")
            {
                ClickByAccessibilityId("minusButton");
            }
            else if (opr == "multiply")
            {
                ClickByAccessibilityId("multiplyButton");
            }
            else if (opr == "divide")
            {
                ClickByAccessibilityId("divideButton");
            }

            else if (opr == "clear")
            {
                ClickByAccessibilityId("clearButton");
            }
            else if (opr == "memoryplus")
            {
                ClickByAccessibilityId("MemPlus");
            }
            else if (opr == "memoryrecall")
            {
                ClickByAccessibilityId("MemRecall");
            }
            else if (opr == "square root")
            {
                ClickByAccessibilityId("squareRootButton");
            }
            else if (opr == "history")
            {
                ClickByAccessibilityId("HistoryLabel");
                waitForSecond(2);
                ClickByClassName("ListViewItem");
            }
        }
        public static void waitForSecond(int timeInSecond)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeInSecond);

        }
        public static void WaitForElementVisible(By by)
        {
            WebDriverWait wt = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wt.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
            wt.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));

        }
     
        public static void closeCalc()
        {
            if (driver != null)
            {
                driver.Quit();
                driver = null;
            }
        }
    }
}
